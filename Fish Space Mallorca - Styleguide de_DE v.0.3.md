# Fish Space Mallorca - Styleguide

#### Einleitung

* *Version: 0.3*
* *Author: Michael Schulze*
* *Copyright: 2011 - 2012 Michael Schulze, Gregor Biewendt*
* *License: All rights reserved. No usage without written permission*
* *Language: de_DE (german)*
* *Last modified: Date: 2012-02-18 14:14:56 +0100 (Sa., 18 Feb 2012)*

### Verwendungszweck

Dieser Styleguide enthält Vorgaben zur grafischen Darstellung (Corporate Design) des Fish Space Mallorca in allen Medien. Alle Vorgaben müssen eingehalten werden. Abweichungen sind stets mit dem Urheber im Vorhinein schriftlich zu vereinbaren.

### Urheberrecht und Nutzungsrechte

#### für dieses Dokument

Alle in diesem Dokument verwendeten Inhalte (Texte, Bilder, Grafiken und Quelltexte [HTML, CSS, JavaScript etc.]) unterliegen dem deutschen Urheber- und Leistungsschutzrecht. Jede vom deutschen Urheber- und Leistungsschutzrecht nicht zugelassene Verwertung bedarf der vorherigen schriftlichen Zustimmung des Anbieters oder jeweiligen Rechteinhabers. Dies gilt insbesondere für Vervielfältigung, Bearbeitung, Übersetzung, Einspeicherung, Verarbeitung bzw. Wiedergabe von Inhalten in Datenbanken oder anderen elektronischen Medien und Systemen. Inhalte und Rechte Dritter sind dabei als solche gekennzeichnet. Die unerlaubte Vervielfältigung oder Weitergabe einzelner Inhalte oder kompletter Abschnitte ist nicht gestattet und strafbar. Lediglich die Herstellung von Kopien und Downloads für den persönlichen, privaten und nicht kommerziellen Gebrauch ist erlaubt.

#### für Logound Layouts (Geschmacksmuster)

Auch das Logo in allen Versionen sowie

Fish Space ist eine eingetragene Wort-/Bildmarke (Bildmarke als Gemeinschaftsmarke). Eine Verwertung, auch von signifikanten Bestandteilen des Logos, insbesondere sofern diese in deren neuer Verwertung auf das ursprüngliche Logo schließen lassen, sind untersagt.

Die Marke ist eingetragen auf:

Lars Gregor Biewendt
Calle Boulevard 47-49, Paguera Center, Local No. 9
E-07160 Paguera ESPAÑA, ES

##### Markenregister

[Nummer der Marke: 010237535 (Link zur Registerauskunft)](http://register.dpma.de/DPMAregister/marke/registerHABM?AKZ=010237535&CURSOR=0)

## Farben

### Hauptfarben

Die Farben aus dem Logo können auch in Layouts oder Designelementen verwendet werden.

#### Lila (1. Hauptfarbe)

![Lila](http://dummyimage.com/400x16:9/5e0953/ffffff&text=Lila "Lila")

* **CMYK:** 40 / 100 / 0 / 45
* **Sonderfarbe:** Pantone 2425C
* **RGB:** 94 / 9 / 82
* **Hexadezimal:** #5e0952

*Von der Verwendung als Hintergrundfarbe oder großen Farbflächen sollte abgesehen werden.*

#### Beige (2. Hauptfarbe)

![Beige](http://dummyimage.com/400x16:9/97874b/ffffff&text=Beige "Beige")

* **CMYK:** 40 / 40 / 80 / 10
* **Sonderfarbe:** Pantone 4505C
* **RGB:** 151 / 135 / 75
* **Hexadezimal:** #97874b

### Weitere Farben

#### Schwarz und Abstufungen (grau)

##### Print - CMYK

* **schwarz (deutsch):** 0 / 0 / 0 / 100
* **grau (spanisch):** 0 / 0 / 0 / 85
* **grau (englisch):** 0 / 0 / 0 / 70

*Die Farbabstufungen in die unterschiedlichen Grautöne kommt ausschließlich zur Anwendung wenn in einem Dokument mehrere Sprachen verwendet werden.*

##### Digital - RGB

* **schwarz (Texte):**  #222222 (Hex), 34 / 34 / 34 (RGB)

## Schriften

### Hausschrift

#### Myriad Pro

Die Myriad Pro wird als Standard-Schrift für die Außendarstellung verwendet. Dies gilt für alle Print-Produkte wie Flyer, Visitenkarten, Plakate, Briefbogen, Anzeigen u.v.m.

##### Schriftschnitte

* **Regular:** Fließtexte, Briefe und Anschreiben
* **Italic:** Hervorhebung einzelner Worte oder Zitate
* **Semibold:** Einleitungen, Fließtexte in Ausnahmen
* **Bold:** Überschriften
* **SemiCondensed, Semibold SemiCondensed, Semibold SemiCondensed Italic, Bold SemiCondensed, Bold SemiCondensed Italic:** als Auszeichnung in Grafiken (z.B. Anfahrtsskizzen) oder Bildunterschriften

### Webfont

Die Hausschrift **Myriad Pro** ist nicht für die Verwendung im Internet lizensiert und so muss hier ein alternativer Webfont verwendet werden.

Der Webfont CartoGothic Std ist eine Schrift, welche für die Verwendung im Internet optimiert und unter einer freien Lizenz veröffentlicht ist. Des weiteren weist dieser Webfont eine hohe Ähnlichkeit zur Schrift Myriad Pro auf. CartoGothic sollte somit auf Webseiten des Fish Space verwendet werden.

Die Schrift kann bei [fontsquirrel.com](http://www.fontsquirrel.com/fonts/CartoGothic-Std) kostenfrei herunter geladen werden.

### Claim

*Fish up YOUR life*

#### Riesling

Die Schrift Riesling wird im Logo für den Buchstaben S und im Claim "Fish up YOUR life" verwendet.

Für eine weitere Verwendung in Fließtexten, Anzeigen oder sonstigen wichtigen Informationen ist dieser Font allerdings ungeeignet.

Die Schrift kann bei [fontsquirrel.com](http://www.fontsquirrel.com/fonts/Riesling) kostenfrei herunter geladen werden.

## Logo

### Versionen

Das Logo kann in zwei Versionen verwendet werden.

#### Version 1 (mit Schrift):

![Standard Logo](assets/images/fishspace-mallorca_logo_font-unten_paths.png "Standard Logo")

*Dateiname: fishspace-mallorca_logo_font-unten_paths (.ai, .eps, .png)*

#### Version 2 (ohne Schrift):

![Standard Logo ohne Schrift](assets/images/fishspace_logo.png "Standard Logo ohne Schrift")

*Dateiname: fishspace_logo (.ai, .eps, .png)*

### Logo als Marke

#### Logo als spezielle Version für Marke

Für die Registrierung als Marke wurde das Logo mit dem Zusatz by Gregor B. und ohne Mallorca erstellt:

![Logo als Marke mit Unterschrift](assets/images/fishspace_logo_font-unten-sign.png "Logo als Marke mit Unterschrift")

*Dateiname: fishspace_logo_font-unten-sign (.ai, .eps, .png)*

## Layout

### Designelemente

Verschiedene Designelemente sollen den Wiedererkennungswert neben dem Logo und den Farben steigern. Die folgenden Elemente sind nicht obligatorisch und können je nach Gestaltungsraster beliebig hinzugefügt oder weggelassen werden.

#### Welle

Eine angedeutete Welle kann in Hintergrundbildern oder als Trenner verschiedener Layoutbereiche (z.B. Goldener Schnitt) als wiederkehrendes, dezent wirkendes Element verwendet werden.

In der Wertigkeit dürfen Designlemente nicht das Logo als solches oder den vermittelten Inhalt (Fotos, Texte) übertreffen und sind stets dezent zu platzieren.

![Welle in einem Plakat](assets/images/welle-plakat_beispiel.jpg "Welle in einem Plakat")

*Beispiel in einem Plakat*

## ToDo's für Styleguide

Weitere Abschnitte und Definitionen, welche noch ergänzt werden müssen.

* Weitere Designelemente
* Abstände zwischen Elementen, Logo und Text
* Mindestgröße von Logos in verschiedenen Medien
* Vorlagen für gängige Medien integrieren (Plakat, Anzeigen, Online-Banner)
* Schwarz/Weiß-Definition (z.B. für Faxe)