###*
 * plugins file with some jQuery plugins and standard functions
 *
 * @author             Michael Schulze
 * @version            $1.0$
 * @copyright          Michael Schulze <elsigno.de>, 29 December, 2011
 * @license            All rights reserved. No usage without written permission.
 * @package            coffeescript, jquery
 * @requirements       jquery-1.7.1.min.js
 *
 * @lastmodified       $Date: 2011-12-29 21:45:44  +0100 (Thu, 29 Dec 2011) $
 *
###

###*
usage: log('inside coolFunc', this, arguments);
paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
###
window.log = ->
  log.history = log.history or []
  log.history.push arguments
  if @console
    arguments.callee = arguments.callee.caller
    console.log Array::slice.call(arguments)

###
make it safe to use console.log always
###
((b) ->
  c = ->
  d = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(",")
  while a = d.pop()
    b[a] = b[a] or c
) window.console = window.console or {}

( ($) ->
   # Accessifyhtml5.js
   # Adds ARIA to new elements in browsers which don’t do it by themselves.
   #
   # originally by Eric Eggert
   # https://github.com/yatil/accessifyhtml5.js/blob/master/accessifyhtml5.js
	$.accessifyhtml5 = ( options ) ->
		fixes =
			'header.site'   : { 'role':          'banner'        }
			'footer.site'   : { 'role':          'contentinfo'   }
			'article'       : { 'role':          'article'       }
			'aside'         : { 'role':          'complementary' }
			'nav'           : { 'role':          'navigation'    }
			'output'        : { 'aria-live':     'polite'        }
			'section'       : { 'role':          'region'        }
			'[required]'    : { 'aria-required': 'true'          }

		$.each(fixes, (index, item) ->
			$(index).attr(item)
		)

		true

	firstplugin =
    init : ( options ) ->
      defaults = 
        resize: 1
      o = $.extend(defaults, options || {})
      firstload = 1
      el = this

      # active update function if window is resized
      if o.resize == 1
        $(window).resize( ->
          firstLoad = 0
          firstplugin.update(o)
          true
        )

        this.each( ->
          # place your plugin code here
        )

    update: (o) ->
      el.each( ->
        # place your plugin code for the update function here
      )

  $.fn.firstplugin = ( method ) ->
    # Method calling logic
    if firstplugin[method]
      firstplugin[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ))
    else if typeof method == 'object' or ! method
      firstplugin.init.apply( this, arguments )
    else
      $.error( 'Method ' +  method + ' does not exist on jQuery.firstplugin' )

  $.secondplugin = ( options ) ->
    defaults =
      resize: 1
    o = $.extend(defaults, options || {})

    # place your plugin code here

    true

  debug = (msg, info) ->
    if debugMode and window.console and window.console.log
      if info
        window.console.log info + ": ", msg
      else
        window.console.log msg
  debugMode = true
  $.fn.extend
    getUniqueId: (p, q, r) ->
      if r is `undefined`
        r = ""
      else
        r = "-" + r
      p + q + r

    accessibleTabs: (config) ->
      defaults =
        wrapperClass: "content"
        currentClass: "current"
        tabhead: "h4"
        tabheadClass: "tabhead"
        tabbody: ".tabbody"
        fx: "show"
        fxspeed: "normal"
        currentInfoText: "current tab: "
        currentInfoPosition: "prepend"
        currentInfoClass: "current-info"
        tabsListClass: "tabs-list"
        syncheights: false
        syncHeightMethodName: "syncHeight"
        cssClassAvailable: false
        saveState: false
        autoAnchor: false
        pagination: false
        position: "top"
        wrapInnerNavLinks: ""
        firstNavItemClass: "first"
        lastNavItemClass: "last"

      keyCodes =
        37: -1
        38: -1
        39: +1
        40: +1

      positions =
        top: "prepend"
        bottom: "append"

      @options = $.extend(defaults, config)
      o = this
      @each (t) ->
        el = $(this)
        list = ""
        tabCount = 0
        ids = []
        $(el).wrapInner "<div class=\"" + o.options.wrapperClass + "\"></div>"
        $(el).find(o.options.tabhead).each (i) ->
          id = ""
          elId = $(this).attr("id")
          if elId
            return  if elId.indexOf("accessibletabscontent") is 0
            id = " id=\"" + elId + "\""
          tabId = o.getUniqueId("accessibletabscontent", t, i)
          navItemId = o.getUniqueId("accessibletabsnavigation", t, i)
          ids.push tabId
          if o.options.cssClassAvailable is true
            cssClass = ""
            if $(this).attr("class")
              cssClass = $(this).attr("class")
              cssClass = " class=\"" + cssClass + "\""
              list += "<li id=\"" + navItemId + "\"><a" + id + "" + cssClass + " href=\"#" + tabId + "\">" + $(this).html() + "</a></li>"
          else
            list += "<li id=\"" + navItemId + "\"><a" + id + " href=\"#" + tabId + "\">" + $(this).html() + "</a></li>"
          $(this).attr
            id: tabId
            class: o.options.tabheadClass
            tabindex: "-1"

          tabCount++

        if o.options.syncheights and $.fn[o.options.syncHeightMethodName]
          $(el).find(o.options.tabbody)[o.options.syncHeightMethodName]()
          $(window).resize ->
            $(el).find(o.options.tabbody)[o.options.syncHeightMethodName]()
        tabs_selector = "." + o.options.tabsListClass
        $(el)[positions[o.options.position]] "<ul class=\"clearfix " + o.options.tabsListClass + " tabamount" + tabCount + "\"></ul>"  unless $(el).find(tabs_selector).length
        $(el).find(tabs_selector).append list
        $(el).find(o.options.tabbody).hide()
        $(el).find(o.options.tabbody + ":first").show()
        $(el).find("ul." + o.options.tabsListClass + ">li:first").addClass(o.options.currentClass).addClass(o.options.firstNavItemClass).find("a")[o.options.currentInfoPosition]("<span class=\"" + o.options.currentInfoClass + "\">" + o.options.currentInfoText + "</span>").parents("ul." + o.options.tabsListClass).children("li:last").addClass o.options.lastNavItemClass
        $(el).find("ul." + o.options.tabsListClass + ">li>a").wrapInner o.options.wrapInnerNavLinks  if o.options.wrapInnerNavLinks
        $(el).find("ul." + o.options.tabsListClass + ">li>a").each (i) ->
          $(this).click (event) ->
            event.preventDefault()
            el.trigger "showTab.accessibleTabs", [ $(event.target) ]
            $.cookie "accessibletab_" + el.attr("id") + "_active", i  if o.options.saveState and $.cookie
            $(el).find("ul." + o.options.tabsListClass + ">li." + o.options.currentClass).removeClass(o.options.currentClass).find("span." + o.options.currentInfoClass).remove()
            $(this).blur()
            $(el).find(o.options.tabbody + ":visible").hide()
            $(el).find(o.options.tabbody).eq(i)[o.options.fx] o.options.fxspeed
            $(this)[o.options.currentInfoPosition]("<span class=\"" + o.options.currentInfoClass + "\">" + o.options.currentInfoText + "</span>").parent().addClass o.options.currentClass
            $($(this).attr("href")).focus().keyup (event) ->
              if keyCodes[event.keyCode]
                o.showAccessibleTab i + keyCodes[event.keyCode]
                $(this).unbind "keyup"

          $(this).focus (event) ->
            $(document).keyup (event) ->
              o.showAccessibleTab i + keyCodes[event.keyCode]  if keyCodes[event.keyCode]

          $(this).blur (event) ->
            $(document).unbind "keyup"

        if o.options.saveState and $.cookie
          savedState = $.cookie("accessibletab_" + el.attr("id") + "_active")
          debug $.cookie("accessibletab_" + el.attr("id") + "_active")
          o.showAccessibleTab savedState, el.attr("id")  if savedState isnt null
        if o.options.autoAnchor and window.location.hash
          anchorTab = $("." + o.options.tabsListClass).find(window.location.hash)
          anchorTab.click()  if anchorTab.size()
        if o.options.pagination
          m = "<ul class=\"pagination\">"
          m += "    <li class=\"previous\"><a href=\"#{previousAnchor}\"><span>{previousHeadline}</span></a></li>"
          m += "    <li class=\"next\"><a href=\"#{nextAnchor}\"><span>{nextHeadline}</span></a></li>"
          m += "</ul>"
          tabs = $(el).find(".tabbody")
          tabcount = tabs.size()
          tabs.each (idx) ->
            $(this).append m
            next = idx + 1
            next = 0  if next >= tabcount
            previous = idx - 1
            previous = tabcount - 1  if previous < 0
            p = $(this).find(".pagination")
            previousEl = p.find(".previous")
            previousEl.find("span").text $("#" + ids[previous]).text()
            previousEl.find("a").attr("href", "#" + ids[previous]).click (event) ->
              event.preventDefault()
              $(el).find(".tabs-list a").eq(previous).click()

            nextEl = p.find(".next")
            nextEl.find("span").text $("#" + ids[next]).text()
            nextEl.find("a").attr("href", "#" + ids[next]).click (event) ->
              event.preventDefault()
              $(el).find(".tabs-list a").eq(next).click()

    showAccessibleTab: (index, id) ->
      debug "showAccessibleTab"
      o = this
      if id
        el = $("#" + id)
        links = el.find("ul." + o.options.tabsListClass + ">li>a")
        el.trigger "showTab.accessibleTabs", [ links.eq(index) ]
        links.eq(index).click()
      else
        @each ->
          el = $(this)
          el.trigger "showTab.accessibleTabs"
          links = el.find("ul." + o.options.tabsListClass + ">li>a")
          el.trigger "showTab.accessibleTabs", [ links.eq(index) ]
          links.eq(index).click()

    showAccessibleTabSelector: (selector) ->
      debug "showAccessibleTabSelector"
      o = this
      el = $(selector)
      if el
        if el.get(0).nodeName.toLowerCase() is "a"
          el.click()
        else
          debug "the selector of a showAccessibleTabSelector() call needs to point to a tabs headline!"


  true

) jQuery

###*
 * "Yet Another Multicolumn Layout" - (X)HTML/CSS Framework
 *
 * (en) Workaround for IE8 und Webkit browsers to fix focus problems when using skiplinks
 * (de) Workaround für IE8 und Webkit browser, um den Focus zu korrigieren, bei Verwendung von Skiplinks
 *
 * @note			inspired by Paul Ratcliffe's article 
 *					http://www.communis.co.uk/blog/2009-06-02-skip-links-chrome-safari-and-added-wai-aria
 *                  Many thanks to Mathias Schäfer (http://molily.de/) for his code improvements
 *
 * @copyright       Copyright 2005-2011, Dirk Jesse
 * @license         CC-A 2.0 (http://creativecommons.org/licenses/by/2.0/),
 *                  YAML-C (http://www.yaml.de/en/license/license-conditions.html)
 * @link            http://www.yaml.de
 * @package         yaml
 * @version         3.3.1
 * @revision        $Revision: 501 $
 * @lastmodified    $Date: 2011-06-18 17:27:44 +0200 (Sa, 18 Jun 2011) $
###
( ->
	YAML_focusFix =
		skipClass : 'skip'
		
		init : ->
			userAgent = navigator.userAgent.toLowerCase()
			is_webkit = userAgent.indexOf('webkit') > -1
			is_ie = userAgent.indexOf('msie') > -1
			
			if is_webkit or is_ie
				body = document.body
				handler = YAML_focusFix.click
				if body.addEventListener
					body.addEventListener('click', handler, false)
				else if body.attachEvent
					body.attachEvent('onclick', handler)
		
		trim : (str) ->
			str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
		
		click : (e) ->
      e = e || window.event
      target = e.target || e.srcElement
      a = target.className.split(' ')
			
      for cls in a
        cls = YAML_focusFix.trim(a[_i])
        if cls == YAML_focusFix.skipClass
          YAML_focusFix.focus(target)
          break
		
		focus : (link) ->
			if link.href
				href = link.href
				id = href.substr(href.indexOf('#') + 1)
				target = document.getElementById(id)
				if target
					target.setAttribute("tabindex", "-1")
					target.focus()

	YAML_focusFix.init()
)()

###*
 * JavaScript email encrypter
 *
 * @author             Michael Schulze
 * @version            $1.0$
 * @copyright          Michael Schulze, 31 December, 2011
 * @license            GNU General Public License, version 3 (GPL-3.0)
 * @package            coffeescript
 *
 * @lastmodified       $Date: 2011-12-31 20:29:35  +0100 (Sat, 31 Dec 2011) $
 *
###

( (window) ->

  ###*
   * Crypt given mail
   *
   * @param string email address
   * @param boolean true
  ###
  window.CryptMailto = ->
    formname = 'cryptmail'
    cryptform = document.forms[formname]
    n = 0
    r = ""
    s = "mailto:" + cryptform.cryptmail_email.value
    e = cryptform.cryptmail_email.value
    
    if cryptform.cryptmail_email.value.length < 4
      return false
    
    radioObj = cryptform.cryptmail_radio
    if radioObj.length > 0
      i = 0
      while i < radioObj.length
        radioValue = parseInt(radioObj[i].value if radioObj[i].checked, 0)
        i++
    else
      radioValue = 0
    
    if radioValue is 1
      e = e.replace(/\./g, '<span class="crypt">.</span>.</span class="crypt">.</span>')
      e = e.replace(/@/, '<span class="crypt">.</span>@</span class="crypt">.</span>')
    else
      e = e.replace(/\./g, ' [dot] ')
      e = e.replace(/@/, ' [at] ')
    
    i = 0
    while i < s.length
      n = s.charCodeAt(i)
      n = 128  if n >= 8364
      r += String.fromCharCode(n + 1)
      i++
    cryptform.cryptmail_cryptedmail.value = r
    cryptform.cryptmail_html.value = '<a href="javascript:linkTo_UnCryptMailto(\'' + r + '\');">' + "\n\t" + e + "\n" + '</a>'
    true

  ###*
   * Uncrypt the email address and returns the valid href
   *
   * @param string the crypted string
   * @return string valid href
  ###
  UnCryptMailto = (s) ->
    n = 0
    r = ""
    i = 0

    while i < s.length
      n = s.charCodeAt(i)
      n = 128  if n >= 8364
      r += String.fromCharCode(n - 1)
      i++
    r

  ###*
   * Public function for A tags
   *
   * @param string the crypted string
   * @return boolean true
  ###
  window.linkTo_UnCryptMailto = (s) ->
    location.href = UnCryptMailto(s)
    true
  
  true

) window